<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    protected $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts =$this->post->paginate(5);

        $postCollection = new PostCollection($posts);
        $postsResource = PostResource::collection($posts)->response()->getData(true);
        return $this->sendSuccessResponse($postCollection, 'Get list success', Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request)
    {
        $dataCreate = $request->all();
        $post = $this->post->create($dataCreate);
        $postResource =new PostResource($post);
        return $this->sendSuccessResponse($postResource, 'Create success', Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post =$this->post->findOrFail($id);

        $postResource =new PostResource($post);

        return $this->sendSuccessResponse($postResource, 'Get post success', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePostRequest $request, string $id)
    {
        $post =$this->post->findOrFail($id);

        $dataUpdate = $request->all();

        $post->update($dataUpdate);

        $postResource =new PostResource($post);

        return $this->sendSuccessResponse($postResource, 'Update success', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post =$this->post->findOrFail($id);
        $post->delete();
        $postResource =new PostResource($post);

        return $this->sendSuccessResponse($postResource, 'Delete success', Response::HTTP_NO_CONTENT);
    }
}
